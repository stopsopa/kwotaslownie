# Kwota słownie (PL)

***

> Biblioteka napisana w języku PHP do słownego zapisu kwoty w języku polskim.

***

* obecnie biblioteka obsługuje sekstyliardy jeśli zajdzie taka potrzeba (co wątpliwe) można w kodzie dopisać w prosty sposób (dopisać do tablicy) odmiany przez przypadki wyższych liczb i będzie działać dalej.


### Użycie:

```php
<?php  
require_once './Lib/Kwota.php';
echo Kwota::getInstance()->slownie('12345678901234567890.45');
```

**Argumenty metody** ->slownie($kwota,$intEnd,$float,$floatEnd):


   * @ param float|int        $kwota    - '5435.7665' || 4321.55 || 432 || .45 
   * @ param null|string      $intEnd   (def: null)
       * null  -> 'złoty,złote,złotych'
       * false -> ',,'
   * @ param bool|null        $float   (def: true)  
       * null, -> ''
       * false -> zwraca '67/100'
       * true -> zwraca 'sześćdziesiąt siedem'
   * @ param null|true|string $floatEnd  (def: null)
       * null  -> 'grosz,grosze,groszy'
       * false -> ',,'


### przykłady:

```php
<?php
require_once './Kwota.php';
echo Kwota::getInstance()->slownie('12345678901234567890.45');
```

zwraca:

    dwanaście trylionów trzysta czterdzieści pięć biliardów sześćset siedemdziesiąt osiem bilionów dziewięćset jeden miliardów dwieście trzydzieści cztery miliony pięćset sześćdziesiąt siedem tysięcy osiemset dziewięćdziesiąt złotych czterdzieści pięć groszy

-------------------

```php
<?php
    echo Kwota::getInstance()->slownie('12345678901234567890.45',false,false,'PLN');
```

zwraca:

    dwanaście trylionów trzysta czterdzieści pięć biliardów sześćset siedemdziesiąt osiem bilionów dziewięćset jeden miliardów dwieście trzydzieści cztery miliony pięćset sześćdziesiąt siedem tysięcy osiemset dziewięćdziesiąt 45/100 PLN
    
# Czas temu (PL)

***

> Biblioteka napisana w języku PHP do słownego zapisu różnicy midzy dwiema datami

***

### Opis pracy biblioteki:

Biblioteka na podstawie obiektu [DateInterval](http://php.net/manual/pl/class.dateinterval.php) lub na podstawie dwóch obiektów [DateTime](http://www.php.net/manual/pl/class.datetime.php) zwraca stringi typu: 

 * sekundę temu
 * 2 sekundy temu
 * 5 sekund temu
 * minutę temu
 * 2 minuty temu
 * 5 minut temu
 * godzinę temu
 * 2 godziny temu
 * 5 godzin temu
 * dzień temu
 * 2 dni temu
 * 5 dni temu
 * tydzień temu
 * 2 tygodnie temu
 * 5 tygodni temu
 * miesiąc temu
 * 2 miesiące temu
 * 5 miesicy temu
 * rok temu
 * 2 lata temu
 * 5 lat temu

### Użycie:

```php
<?php  
require_once './Lib/CzasTemu.php';
echo CzasTemu::getInstance()->get($date1, $date2, 'temu'); // 2 tygodnie temu
echo CzasTemu::getInstance()->get($date1, $date2);         // 2 tygodnie
echo CzasTemu::getInstance()->get($date1,'wstecz');        // 2 tygodnie wstecz
echo CzasTemu::getInstance()->get($interval);              // 2 tygodnie
echo CzasTemu::getInstance()->get($interval,'wstecz');     // 2 tygodnie wstecz
```


# License

The MIT License (MIT)

Copyright (c) 2014 Szymon Działowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.



